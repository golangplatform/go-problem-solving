package main

import (
	"fmt"
	"time"
)

/*
Problem Statement:-
Write a golang program/routine to print odd and even numbers in a order
*/

func main() {
	odd := make(chan bool)
	even := make(chan bool)
	go printEven(50, odd, even)
	go printOdd(50, odd, even)
	time.Sleep(10 * time.Second)
}

func printOdd(n int, odd chan bool, even chan bool) {
	for i := 0; i < n; i++ {
		if i%2 != 0 {
			<-odd
			fmt.Println(i)
			even <- true
		}
	}
}

func printEven(n int, odd chan bool, even chan bool) {
	for i := 0; i < n; i++ {
		if i == 0 {
			fmt.Println(i)
			odd <- true
		}
		if i%2 == 0 && i != 0 {
			<-even
			fmt.Println(i)
			odd <- true
		}
	}
}
