package main

import "fmt"

/*
Problem Statement:-
Given an array, rotate the array to the right by k steps, where k is non-negative.

Follow up:

Try to come up as many solutions as you can, there are at least 3 different ways to solve this problem.
Could you do it in-place with O(1) extra space?


Example 1:

Input: nums = [1,2,3,4,5,6,7], k = 3
Output: [5,6,7,1,2,3,4]
Explanation:
rotate 1 steps to the right: [7,1,2,3,4,5,6]
rotate 2 steps to the right: [6,7,1,2,3,4,5]
rotate 3 steps to the right: [5,6,7,1,2,3,4]
*/

func main() {
	fmt.Println("Rotate Array")
	nums := []int{1, 2, 3, 4, 5, 6, 7}
	k := 3
	rotate(nums, k)
	fmt.Println("Inside main function")
	for i := 0; i < len(nums); i++ {
		fmt.Println(nums[i])
	}
}

func rotate(nums []int, k int) {
	for i := 0; i < k; i++ {
		fmt.Println("Iteration -", i)
		fmt.Println(nums[:(len(nums) - 1)])
		fmt.Println(nums[(len(nums) - 1):])
		nums = append(nums[(len(nums)-1):], nums[:(len(nums)-1)]...)
	}
	fmt.Println("Inside rotate function")
	for i := 0; i < len(nums); i++ {
		fmt.Println(nums[i])
	}
}
