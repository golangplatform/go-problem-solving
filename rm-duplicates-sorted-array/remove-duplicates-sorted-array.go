package main

import "fmt"

/*
Problem Statement:-
Given a sorted array nums, remove the duplicates in-place such that each element appears only once and returns the new length.
Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

Example 1:
Given nums = [1,1,2],
Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.
It doesn't matter what you leave beyond the returned length.
*/

/*
About in-place algorithm
In computer science, an in-place algorithm is an algorithm which transforms input using no auxiliary data structure. However a small amount of extra storage space is allowed for auxiliary variables. The input is usually overwritten by the output as the algorithm executes. In-place algorithm updates input sequence only through replacement or swapping of elements.
An algorithm which is not in-place is sometimes called not-in-place or out-of-place.
*/

func main() {
	fmt.Println("Remove Duplicates from Sorted Array")
	nums := []int{0, 1, 1, 1, 2, 2, 3, 3, 4}

	// nums is passed in by reference. (i.e., without making a copy)
	len := removeDuplicates(nums)

	// any modification to nums in your function would be known by the caller.
	// using the length returned by your function, it prints the first len elements.

	for i := 0; i < len; i++ {
		fmt.Println(nums[i])
	}
}

func removeDuplicates(nums []int) int {
	i := 0
	for j := 1; j < len(nums); {
		if nums[i] == nums[j] {
			fmt.Println("Iteration -", j)
			fmt.Println(nums[:i+1])
			fmt.Println(nums[j+1:])
			nums = append(nums[:i+1], nums[j+1:]...)
			fmt.Println(nums)
		} else {
			i++
			j++
		}
	}
	return len(nums)
}
