package main

import (
	"fmt"
	"strconv"
	"strings"
)

/*
Problem Statement:-
Task
A format for expressing an ordered list of integers is to use a comma separated list of either

individual integers
or a range of integers denoted by the starting integer separated from the end integer in the
range by a dash, '-'. The range includes all integers in the interval including both endpoints.
It is not considered a range unless it spans at least 3 numbers. For example "12,13,15-17".
Complete the solution so that it takes a list of integers in increasing order and returns
a correctly formatted string in the range format.

Documentation
solution(list)
Parameters
list: []int - An ordered list of unique integers

Return Value
String - formatted string with range format

Example:

Solution([]int{-6,-3,-2,-1,0,1,3,4,5,7,8,9,10,11,14,15,17,18,19,20})
// returns "-6,-3-1,3-5,7-11,14,15,17-20"
*/

func main() {
	nums := []int{-6, -3, -2, -1, 0, 1, 3, 4, 5, 7, 8, 9, 10, 11, 14, 15, 17, 18, 19, 20}
	res := rangeFormat(nums)
	fmt.Print(res)
}

func rangeFormat(integers []int) string {

	// res := integers
	// for i := 0; i < len(integers); i++ {
	// 	if i == 0 || i == len(integers)-1 {
	// 		continue
	// 	}
	// 	fmt.Println("Iteration", i)
	// 	fmt.Println(res[i] - 1)
	// 	fmt.Println(integers[i-1])
	// 	fmt.Println(res[i] + 1)
	// 	fmt.Println(integers[i+1])
	// 	if res[i]-1 == integers[i-1] && res[i]+1 == integers[i+1] {
	// 		res[i] = 0
	// 	}

	// }

	//fmt.Print(res)
	//return strings.Replace(strings.Join(res), "-")

	rangeString := []string{}
	counter := 0
	for i := 0; i < len(integers); i++ {
		fmt.Println("Iteration", i)
		counter = 0
		init := strconv.Itoa(integers[i])
		fmt.Println("init", init)
		fmt.Println("integers[i+1]-integers[i]", integers[i+1]-integers[i])
		for i != len(integers)-1 && integers[i+1]-integers[i] == 1 {
			counter++
			i++
		}

		if counter > 0 {
			if counter > 1 {
				initInt, _ := strconv.Atoi(init)
				init = init + "-" + strconv.Itoa(initInt+counter)
				fmt.Println("init inside counter", init)
			} else {
				i--
			}
		}
		rangeString = append(rangeString, init)
		fmt.Println("rangeString", rangeString)

	}
	return strings.Join(rangeString, ",")
}
