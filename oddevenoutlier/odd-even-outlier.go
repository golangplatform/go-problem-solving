package main

import "fmt"

func main() {
	nums := []int{-3, 6, 8, -10, 4}
	res := FindOutlier(nums)
	fmt.Print(res)
}

func FindOutlier(integers []int) int {
	evenInts := []int{}
	oddInts := []int{}

	for i := 0; i < len(integers); i++ {

		if integers[i]%2 == 0 {

			evenInts = append(evenInts, integers[i])
		} else {
			oddInts = append(oddInts, integers[i])
		}
	}

	fmt.Print(evenInts)
	fmt.Print(oddInts)

	if len(evenInts) > len(oddInts) {
		return oddInts[0]
	} else {
		return evenInts[0]
	}
}
