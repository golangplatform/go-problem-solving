package main

import "fmt"

type Node struct {
	value interface{}
	next  *Node
}

type RLinkedList struct {
	head   *Node
	length int
}

func main() {

	list := RLinkedList{}
	list.head = &Node{value: 1, next: nil}
	list.length = 1

	list.head.next = &Node{value: 2, next: nil}
	list.length = 2

	list.head.next.next = &Node{value: 3, next: nil}
	list.length = 3

	list.head.next.next.next = &Node{value: 4, next: nil}
	list.length = 4

	list.head.next.next.next.next = &Node{value: 5, next: nil}
	list.length = 5

	list.Print()
	list.ReverseList()
	list.Print()
	list.MiddleOfList()
}

func (list *RLinkedList) Print() {

	head := list.head
	for head != nil {
		fmt.Println(head.value)
		head = head.next
	}

}

func (list *RLinkedList) ReverseList() {

	var prevNode *Node
	head := list.head
	var nextNode *Node

	for head != nil {
		nextNode = head.next
		head.next = prevNode
		prevNode = head
		head = nextNode

	}
	list.head = prevNode
}

func (list *RLinkedList) MiddleOfList() {
	current := list.head
	var middleElement *Node
	for i := 0; i < list.length/2; i++ {
		current = current.next
		middleElement = current

	}
	fmt.Println("Mid Element", middleElement.value)
}
