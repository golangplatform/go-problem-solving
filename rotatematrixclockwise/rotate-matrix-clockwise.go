package main

import "fmt"

/*
Problem Statement:-
Write a golang program to rotate 4x4 matrix clockwise
*/

const m = 4
const n = 4

func main() {

	matrix := [m][n]int{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}}
	printMatrix(matrix)
	rotateMatrix(matrix)
}

func printMatrix(matrix [m][n]int) {
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			fmt.Print(matrix[i][j], "\t")
		}
		fmt.Println()
	}
}

// First Iteration
// matrix[0][0] -> matrix[0][3]
// matrix[0][1] -> matrix[1][3]
// matrix[0][2] -> matrix[2][3]
// matrix[0][3] -> matrix[3][3]

// Second Iteration
// matrix[1][0] -> matrix[0][2]
// matrix[1][1] -> matrix[1][2]
// matrix[1][2] -> matrix[2][2]
// matrix[1][3] -> matrix[3][2]

// Third Iteration
// matrix[2][0] -> matrix[0][1]
// matrix[2][1] -> matrix[1][1]
// matrix[2][2] -> matrix[2][1]
// matrix[2][3] -> matrix[3][1]

// Fourth Iteration
// matrix[3][0] -> matrix[0][0]
// matrix[3][1] -> matrix[1][0]
// matrix[3][2] -> matrix[2][0]
// matrix[3][3] -> matrix[3][0]

func rotateMatrix(matrix [m][n]int) {
	var rotatedMatrix [m][n]int
	k := n - 1
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			rotatedMatrix[j][k] = matrix[i][j]
		}
		// fmt.Println("Matrix After Rotation ->", i+1)
		// printMatrix(rotatedMatrix)
		k = n - 1 - (i + 1)
	}
	fmt.Println("Matrix After Rotation")
	printMatrix(rotatedMatrix)

}
